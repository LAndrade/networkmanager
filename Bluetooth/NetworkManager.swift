//
//  NetworkManager.swift
//  Bluetooth
//
//  Created by Leonardo de Andrade Pinheiro on 02/04/15.
//  Copyright (c) 2015 Leonardo de Andrade Pinheiro. All rights reserved.
//

import UIKit
import MultipeerConnectivity

protocol NetworkProtocol {
    func onConnect(peer : MCPeerID)
    func onConnectError(peer : MCPeerID)
    func onReceiveData(data : AnyObject, peerid : MCPeerID, name : String)
}

class NetworkManager: NSObject, MCBrowserViewControllerDelegate {
    var appDelegate:AppDelegate!
    var delegate:NetworkProtocol! = nil
    var peerArray : [String] = []
    var handler : MPCHandler!
    var isHost = false
    var pos = 0

    override init() {
        super.init()
        let uid = randomStringWithLength(5)
        appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        handler = appDelegate.mpcHandler;
        handler.setupPeerWithDisplayName("\(UIDevice.currentDevice().name) [ID: \(uid)]")
        handler.setupSession()
        handler.advertiseSelf(true)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "peerChangedStateWithNotification:", name: "MPC_DidChangeStateNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleReceivedDataWithNotification:", name: "MPC_DidReceiveDataNotification", object: nil)
        
        peerArray.append(handler.peerID.displayName)
        println("Generated Session")
    }
    
    func sendToPeers(destination: [AnyObject], data: Dictionary<String, AnyObject>) {
        let messageDict = data;
        let messageData = NSJSONSerialization.dataWithJSONObject(messageDict, options: NSJSONWritingOptions.PrettyPrinted, error: nil)
        var error:NSError?
        handler.session.sendData(messageData, toPeers: destination, withMode: MCSessionSendDataMode.Reliable, error: &error)
        if error != nil{
            println("error: \(error?.localizedDescription)")
        }
    }
    
    func sendData(destination: String, data: Dictionary<String, AnyObject>) {
        var targetID : [MCPeerID] = [];
        for peer in handler.session.connectedPeers {
            let p = peer as MCPeerID;
            if(p.displayName == destination) {
                targetID.append(p)
                break;
            }
        }
        sendToPeers(targetID, data: data)
    }
    
    func presentBrowser(target : UIViewController) {
        if(handler.session != nil) {
            handler.setupBrowser()
            handler.browser.delegate = self
            target.presentViewController(handler.browser, animated: true, completion: nil)
        }
        println("Broswer Presented")
    }
    
    func peerChangedStateWithNotification(notification:NSNotification){
        let userInfo = NSDictionary(dictionary: notification.userInfo!)
        let state = userInfo.objectForKey("state") as Int
        let pid = userInfo.objectForKey("peerID") as MCPeerID
        if state != MCSessionState.Connecting.rawValue{
            if(state == MCSessionState.Connected.rawValue) {
                peerArray.append(pid.displayName)
                println("New Peer Connected: \(pid.displayName)")
                delegate.onConnect(pid)
            } else {
                println("Peer Failed to Connect: \(pid.displayName)")
                delegate.onConnectError(pid)
            }
        }
    }
    
    func handleReceivedDataWithNotification(notification:NSNotification){
        let userInfo = notification.userInfo! as Dictionary
        let receivedData: NSData = userInfo["data"] as NSData
        let dicData = NSJSONSerialization.JSONObjectWithData(receivedData, options: NSJSONReadingOptions.AllowFragments, error: nil) as NSDictionary
        let senderPeerId: MCPeerID = userInfo["peerID"] as MCPeerID
        let senderDisplayName = senderPeerId.displayName
        println("Data Received")
        if(dicData["peerArr"] != nil) {
            if(!isHost && (dicData.valueForKey("peerArr") as Int) == 0) {
                let arr = dicData.objectForKey("data") as [String]
                peerArray = arr;
                let name = handler.peerID.displayName
                for var x = 0 ; x < peerArray.count ; x++ {
                    if(peerArray[x] == name) {
                        pos = x
                        break
                    }
                }
                println("Set PeerArray with Received Data")
            } else if(isHost) {
                pos = 0
            }
        } else {
            delegate.onReceiveData(dicData, peerid: senderPeerId, name: senderDisplayName)
        }
    }
    
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController!) {
        isHost = true
        println("Browser Closed - Done | isHost set")
        handler.browser.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController!) {
        println("Browser Closed - Cancel | isHost not set")
        handler.browser.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func distributeNetworkPeerArray() {
        for p in handler.session.connectedPeers {
            sendData(p.displayName, data: ["peerArr" : 0, "data" : peerArray])
            println("Sent Peer Array to \(p.displayName)")
        }
        println("Peers Array Distributed")
    }
    
    func randomStringWithLength(len : Int) -> NSString {
        let letters : NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString : NSMutableString = NSMutableString(capacity: len)
        for (var i=0; i < len; i++){
            var length = UInt32 (letters.length)
            var rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        return randomString
    }
    
    // 0 - Init / 1 - (X - 1) - Trans / X - Final
    func getPeerForPos(index : Int) -> MCPeerID {
        let name = peerArray[index]
        for var x = 0 ; x < handler.session.connectedPeers.count ; x++ {
            if(handler.session.connectedPeers[x].displayName == name) {
                return handler.session.connectedPeers[x] as MCPeerID
            }
        }
        var empty = MCPeerID()
        return empty;
    }
}
