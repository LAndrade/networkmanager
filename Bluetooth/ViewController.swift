//
//  ViewController.swift
//  TicTacToe
//
//  Created by Training on 12/09/14.
//  Copyright (c) 2014 Training. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UIViewController, NetworkProtocol {
    
    var manager = NetworkManager();
    @IBOutlet weak var log: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self;
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func distributePeers(sender: AnyObject) {
        manager.distributeNetworkPeerArray()
    }

    func onConnect(peer : MCPeerID) {
        var string = "";
        for p in manager.peerArray {
            string += "\(p)\n"
        }
        log.text = string
        println("Connected")
    }
    
    func onConnectError(peer : MCPeerID) {
        println("Failed to Connect")
    }
    
    func onReceiveData(data: AnyObject, peerid: MCPeerID, name: String) {
        var string = "";
        for p in manager.peerArray {
            string += "\(p)\n"
        }
        log.text = string
    }
    
    @IBAction func connectClick(sender: AnyObject) {
        manager.presentBrowser(self)
    }
}

